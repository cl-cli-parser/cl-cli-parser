(in-package :cl-user)

(defpackage :lunit
  (:use :cl)
  (:export #:deftest #:check #:get-tests))
(in-package :lunit)

;;; from peter seibel's book, practical common lisp
;;; www.gigamonkeys.com/book

(defmacro with-gensyms ((&rest syms) &body body)
  `(let ,(loop for sym in syms collect `(,sym (gensym ,(symbol-name sym))))
     ,@body))

(defvar *test-name* nil)

(defmacro deftest (name parameters &body body)
  "Define a test function. Within a test function we can call
   other test functions or use `check' to run individual test
   cases."
  `(defun ,name ,parameters
    (let ((*test-name* (append *test-name* (list ',name))))
      (macrolet ((check (&body forms)
                   `(combine-results
                      ,@(loop for f in forms collect `(report-result ,f ',f)))))
        ,@body))))

(defmacro combine-results (&body forms)
  "Combine the results (as booleans) of evaluating `forms' in order."
  (with-gensyms (result)
    `(let ((,result t))
      ,@(loop for f in forms collect `(unless ,f (setf ,result nil)))
      ,result)))

(defun report-result (result form)
  "Report the results of a single test case. Called by `check'."
  (format t "~:[FAIL~;pass~] ... ~a: ~w~%" result *test-name* form)
  result)

(defun get-tests (&optional (p *package*))
  "Get a list of the symbols corresponding to unit test functions
from the package P."
  (loop for x being the symbols of p
        if (eql 0 (search "test-" (symbol-name x) :test #'string-equal))
        collect x))